using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZagMovement : MonoBehaviour
{
    [SerializeField] private Transform enemy; 
    private float orgPosition;
    [SerializeField] private float totalChange;
    [SerializeField] private float changeInPosition;
    private bool moveLeft;

    private void Start()
    {
        orgPosition = enemy.position.x;
    }

    private void Update()
    {
        if (enemy.position.x >= orgPosition + totalChange)
        {
            moveLeft = false;
        }

        if (enemy.position.x <= orgPosition - totalChange)
        {
            moveLeft = true;
        }

        if (moveLeft)
        {
            enemy.position += new Vector3(changeInPosition * Time.fixedDeltaTime, 0,0);
        }
        else
        {
            enemy.position -=  new Vector3(changeInPosition * Time.fixedDeltaTime, 0,0);
        }
    }
}
