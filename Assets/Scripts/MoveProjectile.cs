using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveProjectile : MonoBehaviour
{
    [SerializeField]private Rigidbody2D projectile;
    [SerializeField]private float moveSpeed = 10.0f;

    private void Start()
    {
        projectile = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        projectile.velocity = new Vector2(0,-1) * moveSpeed;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Detection" || other.tag == "Bomb")
        {
            Destroy(gameObject);
        }
    }
}
