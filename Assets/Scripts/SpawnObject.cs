using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    [SerializeField]private GameObject[] powerUps;
    [SerializeField] private GameObject[] powerDowns;
    private bool StopSpawning = false;
    
    
    public void StartSpawning()
    {
        StartCoroutine(SpawnPowerUpRoutine());
        StartCoroutine(SpawnPowerDownRoutine());
    }
    

    IEnumerator SpawnPowerUpRoutine()
    {
        yield return new WaitForSeconds(3.0f);
        while (StopSpawning == false)
        {
            Vector3 posToSpawn = new Vector3(Random.Range(-8.0f, 8.0f), -7.5f, 0);
            int RandomPowerUp = Random.Range(0, 4);
            Instantiate(powerUps[RandomPowerUp], posToSpawn, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(3, 10));
        }
    }
    
    IEnumerator SpawnPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        while (StopSpawning == false)
        {
            Vector3 posToSpawn = new Vector3(Random.Range(-8.0f, 8.0f), -7.5f, 0);
            int RandomPowerDown = Random.Range(0, 2);
            Instantiate(powerDowns[RandomPowerDown], posToSpawn, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(3, 10));
        }
    }

   
    
    public void OnPlayerDeath()
    {
        StopSpawning = true;
    }
}
