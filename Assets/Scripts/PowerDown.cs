using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDown : MonoBehaviour
{
    [SerializeField]private int powerDownID;
    [SerializeField]private float speed = 3.0f;
    
    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        if( transform.position.y< -8f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            PlayerController player = other.transform.GetComponent<PlayerController>();
            if (player != null)
            {
                switch (powerDownID)
                {
                    case 3:
                        player.SpeedActive();
                        break;
                    case 4:
                        player.CanMove();
                        break;
                }
                Destroy(this.gameObject);
            }
        }
        
        if (other.tag == "MotherShip")
        {
            Destroy(gameObject);
            
        }
    }
    
    
}
