
using System;
using Damage_Health;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float moveSpeed = 10.0f;
    [SerializeField] private Rigidbody2D bomb;
    [SerializeField] private AudioClip bombExplodeSounds;
    [SerializeField] private float bombExplodeSoundsValume = 0.3f;
    [SerializeField] private GameObject explosionGo;


    private void Start()
    {
        bomb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        bomb.velocity = new Vector2(0,1) * moveSpeed;
    }

    private void Awake()
    {
        Debug.Assert(bomb != null,"rigidbody2D cannot be null");
    }

    private void PlayExplosion()
    {
        GameObject explosion = (GameObject) Instantiate(explosionGo);
        explosion.transform.position = transform.position;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "MotherShip")
        {
            var target = other.gameObject.GetComponent<IDamage>();
            target?.TakeDamage(damage);
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(bombExplodeSounds,Camera.main.transform.position,bombExplodeSoundsValume);
        }

        if (other.tag == "Player Bullet")
        {
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(bombExplodeSounds,Camera.main.transform.position,bombExplodeSoundsValume);
            PlayExplosion();
        }

        if (other.tag == "Player")
        {
            PlayerController player = other.transform.GetComponent<PlayerController>();
            player.Damage();
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(bombExplodeSounds,Camera.main.transform.position,bombExplodeSoundsValume);
        }
        
        
       
        
    }
}