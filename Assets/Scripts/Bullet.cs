
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{
    public Text maxAmmoText;
    [SerializeField] private Transform projectileSpawn;
    [SerializeField] private GameObject projectile;
    [SerializeField] private float maxAmmo;
    [SerializeField] private double ammoRelaodTime;
    private float ammo;
    private double relaodTime;
    private bool isTripleShootActive = false;
    [SerializeField]private GameObject TripleShoot;

    private void Start()
    {
        relaodTime = ammoRelaodTime;
        projectileSpawn = gameObject.transform;
        
        ammo = maxAmmo;
    }

    private void Update()
    {
        Text();
        Shoot();
    }

    private void Text()
    {
        if (ammo>0)
        {
            maxAmmoText.text = "Ammo x "+ ammo ;
        }

        if (ammo<=0)
        {
            maxAmmoText.text = "Reload " + ammoRelaodTime.ToString("0.0") +"s";
        }
    }
    private void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space)||(Input.GetMouseButtonDown(0)))
        {
            if (ammo > 0 && isTripleShootActive == true)
            {
                Instantiate(TripleShoot, projectileSpawn.position, Quaternion.identity);
            }

            if (ammo > 0)
            {
                Instantiate(projectile, projectileSpawn.position, Quaternion.identity);
                --ammo;
            }
        }
        if (ammo<=0 || (Input.GetKeyDown(KeyCode.R)&&maxAmmo>ammo))
        {
            Reload();
        }

    }

    private void Reload()
    {
        ammo = 0;
        ammoRelaodTime -= Time.deltaTime;
        if (ammoRelaodTime <= 0)
        {
            ammo = maxAmmo;
            ammoRelaodTime = relaodTime;
        }
    }

    public void TripleShotActive()
    {
        isTripleShootActive = true;
        StartCoroutine(TripleShotPowerDownRoutine());
    }

    IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(4.0f);
        isTripleShootActive = false;
    }










}


