using System;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private GameObject optionPanel;
        [SerializeField] private GameObject mainMenuPanel;
        [SerializeField] private GameObject creditPanel;
        [SerializeField] private GameObject howToPlay;
        [SerializeField] private GameObject mode;
        [SerializeField] private AudioClip clickedSounds;
        [SerializeField] private float clickedSoundsValue = 0.2f;
        public void StartGame()
        {
             SceneManager.LoadScene("ModeSelect");
        }
    
        public void ShowOptions()
        {
            mainMenuPanel.SetActive(false);
            optionPanel.SetActive(true);
            creditPanel.SetActive(false);
            howToPlay.SetActive(false);
        }
    
        public void ShowMainMenu()
        {
            mainMenuPanel.SetActive(true);
            creditPanel.SetActive(false);
            howToPlay.SetActive(false);
            
        }

        public void ShowCredit()
        {
            mainMenuPanel.SetActive(false);
            creditPanel.SetActive(true);
            howToPlay.SetActive(false);
        }

        public void ShowHowToPlay()
        {
            howToPlay.SetActive(true);
            creditPanel.SetActive(false);
            mainMenuPanel.SetActive(false);
        }
    
        public void Exit()
        {
            Debug.Log("Exit");
            Application.Quit();
        }
            
            
            
    }
}
