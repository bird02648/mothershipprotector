using System;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class SelectMode : MonoBehaviour
    {
        [SerializeField] private GameObject normalMode;
        [SerializeField] private GameObject endlessMode;
        [SerializeField] private GameObject backToMenu;
       

        

        public void NormalMode()
    
        {
            SceneManager.LoadScene("Scene1");
        }
    
        public void EndlessMode()
        {
            SceneManager.LoadScene("EndlessScene");
        }
    
        public void BackToMainMenu()
        {
            SceneManager.LoadScene("Menu");
        }

     
            
    }
}