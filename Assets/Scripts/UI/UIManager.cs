using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Image _LiveImage;
        [SerializeField] private Sprite[]  _liveSprite;
        [SerializeField] GameObject gameOverUI;

        public void UpdateLives( int currentLives)
        {
            _LiveImage.sprite = _liveSprite[currentLives];
            if (currentLives < 1)
            {
                GameOverSequence();
            }

            void GameOverSequence()
            {
                gameOverUI.SetActive(true);
                Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            }
        
        }

    
    }
}
