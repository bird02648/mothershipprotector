using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class PauseMenu : MonoBehaviour
    {
    
        private bool isPaused = false;
        [SerializeField] private GameObject pauseUI;
        [SerializeField] private AudioClip clickedSounds;
        [SerializeField] private float clickedSoundsValume = 0.2f;
        private TimeCount timeCount;
        private UIManager uiManager;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                AudioSource.PlayClipAtPoint(clickedSounds,Camera.main.transform.position,clickedSoundsValume);
                SwitchPause();
            }
        }

        public void SwitchPause()
        {
            pauseUI.SetActive(!pauseUI.activeSelf);
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
        }
    
        public void Menu()
        {
        
            SceneManager.LoadScene("Menu");
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }

        public void RestartScene1()
        {
            SceneManager.LoadScene("Scene1");
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }

        
        public void RestartEndlessScene()
        {
            SceneManager.LoadScene("EndlessScene");
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }
        
        public void GoScene2()
        {
            SceneManager.LoadScene("Scene2");
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }
        
         public void GoScene3() 
         {
             SceneManager.LoadScene("Scene3");
             Time.timeScale = Time.timeScale == 1 ? 0 : 1;
             isPaused = isPaused = true ? false : true;
         }

    
    
    }
}
