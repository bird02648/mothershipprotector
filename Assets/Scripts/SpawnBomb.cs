using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnBomb : MonoBehaviour
{
    [SerializeField] private GameObject bomb;
    [SerializeField] private float maxX;
    [SerializeField] private float minX;
    [SerializeField] private float maxY;
    [SerializeField] private float minY;
    [SerializeField] private float timeBetweenSpawn;
    [SerializeField] private AudioClip spawnBombSounds;
    [SerializeField] private float spawnBombSoundsValume = 0.2f;
    private float spawnTime;

    // Update is called once per frame

    

    void Update()
    {
        if (Time.time > spawnTime)
        {
            Spawn();
            spawnTime = Time.time + timeBetweenSpawn;
        }
    }

    void Spawn()
    {
        float randomX = Random.Range(minX, maxX);
        float randomY = Random.Range(minY, maxY);
        
        Instantiate(bomb, transform.position + new Vector3(randomX, randomY, 0), transform.rotation);
        AudioSource.PlayClipAtPoint(spawnBombSounds,Camera.main.transform.position,spawnBombSoundsValume);
    }
    
}
