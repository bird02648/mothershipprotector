using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.UI;
using Object = System.Object;

public class PlayerController : MonoBehaviour
{
    [SerializeField]private float playerSpeed;
    [SerializeField]private Transform projectileSpawn;
    [SerializeField]private Transform projectileTripleSpawn;
    [SerializeField]private GameObject projectile;
    [SerializeField]private float maxAmmo;
    [SerializeField]private double ammoReloadTime;
    [SerializeField]private GameObject TripleShoot;
    [SerializeField]private GameObject Shield;
    [SerializeField]private float speedMultiplier = 1.3f;
    [SerializeField]private float speedDevider = 2.0f;
    [SerializeField]private int life = 3;
    [SerializeField] private AudioClip playerFireSounds;
    [SerializeField] private float playerFireSoundsValume = 0.1f;
    [SerializeField] private AudioClip powerUpSounds;
    [SerializeField] private float powerUpSoundsValume = 0.2f;
    [SerializeField] private AudioClip playerExplodeSounds;
    [SerializeField] private float playerExplodeSoundsValume = 0.2f;
    private UIManager _uimanager;
    private Rigidbody2D rb;
    private Vector2 playerDirection;
    private bool isSpeedBoostActive = false;
    private bool isTripleShootActive = false;
    private bool isShieldActive = false;
    private bool isSpeedPowerDownActive = false;
    public Text maxAmmoText;
    private float ammo;
    private double reloadTime;
    private float TripleShootCountdown;
    public Text TripleShootText;
    private bool cantMove = false;
    public SpriteRenderer sprite;
    [SerializeField] private AudioClip reloadSounds;
    [SerializeField] private float reloadValume = 0.2f;
    [SerializeField] private AudioClip reloadEmptySounds;
    [SerializeField] private float reloadEmptyValume = 0.2f;



    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        reloadTime = ammoReloadTime;
        ammo = maxAmmo;
        _uimanager = GameObject.Find("Canvas").GetComponent<UIManager>();
    }
    void Update()
    {
        float directionY = Input.GetAxisRaw("Horizontal");
        if (cantMove == false)
        {
            playerDirection = new Vector2(directionY, 0).normalized;
        }
        TextAmmo();
        Shoot();
        TripleShootTextCheck();
        
    }

    private void TripleShootTextCheck()
    {
        if (TripleShootCountdown>=0)
        {
            TripleShootText.text = " : "+ TripleShootCountdown.ToString("0.0");
            TripleShootCountdown -= Time.deltaTime;
        }
        
        // yield return new WaitForSeconds(4.0f);
        else
        {
            TripleShootText.text = "";
            isTripleShootActive = false;
        }
        
    }
    
    private void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space)&&Time.timeScale != 0 ||(Input.GetMouseButtonDown(0))&&Time.timeScale != 0 && cantMove == false)
        {
            if (isTripleShootActive == true && ammo > 0)
            {
                Instantiate(TripleShoot, projectileTripleSpawn.position, Quaternion.identity);
                ammo = ammo - 1;
            }

            if(ammo > 0)
            {
                Instantiate(projectile, projectileSpawn.position, Quaternion.identity);
                --ammo;
                AudioSource.PlayClipAtPoint(playerFireSounds,Camera.main.transform.position,playerFireSoundsValume);
            }
            
        }
        if (ammo<=0 || (Input.GetKeyDown(KeyCode.R)&&maxAmmo>ammo))
        {
            Reload();
             AudioSource.PlayClipAtPoint(reloadSounds,transform.position,reloadValume);
        }
        

    }
    
    private void TextAmmo()
    {
        if (ammo>0)
        {
            maxAmmoText.text = " x "+ ammo ;
        }

        if (ammo<=0)
        {
            maxAmmoText.text = " Reload " + ammoReloadTime.ToString("0.0") +"s";
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(playerDirection.x * playerSpeed, 0);
        if (cantMove == true)
        {
            rb.velocity = Vector2.zero;
        }
    }
    
    private void Reload()
    {
        ammo = 0;
        ammoReloadTime -= Time.deltaTime;
        if (ammoReloadTime <= 0)
        {
            ammo = maxAmmo;
            ammoReloadTime = reloadTime;
        }

        if (ammo <= 0 && Input.GetKeyDown(KeyCode.Space) && Time.timeScale == 1 || ammo <= 0 && Input.GetMouseButtonDown(0) && Time.timeScale == 1)
        {
            AudioSource.PlayClipAtPoint(reloadEmptySounds,Camera.main.transform.position,reloadEmptyValume);
        }
        // if ()
    }
    
    public void Damage()
    {
        if( isShieldActive == true)
        {
            isShieldActive = false;
            Shield.SetActive(false);
            return;
        }
        life--;
        _uimanager.UpdateLives(life);
        StartCoroutine(FlashRed());
        if (ammo <=0 )
        {
            ammoReloadTime = reloadTime;
        }

        if (life < 1)
        {
            Destroy(this.gameObject);
            AudioSource.PlayClipAtPoint(playerExplodeSounds,Camera.main.transform.position,playerExplodeSoundsValume);
        }
    }
    
    public void TripleShotActive()
    {
        isTripleShootActive = true;
        TripleShootCountdown = 4 ;
        TripleShotPowerDownRoutine();
        // StartCoroutine(TripleShotPowerDownRoutine());
        AudioSource.PlayClipAtPoint(powerUpSounds,Camera.main.transform.position,powerUpSoundsValume);
    }

    void TripleShotPowerDownRoutine()
    {
        
      
    }
    
    public void SpeedBoostActive()
    {
        isSpeedBoostActive = true;
        playerSpeed *= speedMultiplier;
        StartCoroutine(SpeedBoostPowerDownRoutine());
        AudioSource.PlayClipAtPoint(powerUpSounds,Camera.main.transform.position,powerUpSoundsValume);
    }
    
    IEnumerator SpeedBoostPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        isSpeedBoostActive = false;
        playerSpeed /= speedMultiplier;
    }
    

    public void ShieldIsActive()
    {
        isShieldActive = true;
        Shield.SetActive(true);
        AudioSource.PlayClipAtPoint(powerUpSounds,Camera.main.transform.position,powerUpSoundsValume);
    }

    public void CanMove()
    {
        cantMove = true;
        StartCoroutine(CantMove());
        AudioSource.PlayClipAtPoint(powerUpSounds,Camera.main.transform.position,powerUpSoundsValume);
    }

    IEnumerator CantMove()
    {
        yield return new WaitForSeconds(1.5f);
        cantMove = false;
    }

    public void SpeedActive()
    {
        isSpeedBoostActive = true;
        playerSpeed /= speedDevider;
        StartCoroutine(SpeedPowerDownRoutine());
        AudioSource.PlayClipAtPoint(powerUpSounds,Camera.main.transform.position,powerUpSoundsValume);
    }

    IEnumerator SpeedPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        isSpeedPowerDownActive = false;
        playerSpeed *= speedDevider;
    }

    public void IncreaseLives()
    {
        if (life < 3)
        {
            life ++;
            _uimanager.UpdateLives(life);
            AudioSource.PlayClipAtPoint(powerUpSounds,Camera.main.transform.position,powerUpSoundsValume);
        }
    }

    public IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }
}
