using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Time;

public class TimeEndless : MonoBehaviour
{
    [SerializeField] private Text textTimer;
    [SerializeField] private float time;
    [SerializeField] private Text textTimeShow;
    private bool gameActive = true;
    private float second;
    private PauseMenu pauseMenu;
    
    
    // [SerializeField]private GameObject winGameUI;
    void Update()
    {
        if (gameActive)
        {
            second += Time.deltaTime;
            if (second >= 1)
            {
                time++;
                second = 0;
            }
        }
        
        // if (gameActive && time <= 0)
        // {
        //     winGameUI.SetActive(true);
        //     gameActive = false;
        //     Time.timeScale = Time.timeScale == 1 ? 0 : 1;
        // }


        SetText();
    }
    

    void SetText()
    {
        int hour = Mathf.FloorToInt(time / 60);
        int minute = Mathf.FloorToInt(time % 60);
        textTimer.text = hour.ToString("00") + ":" + minute.ToString("00");
        textTimeShow.text = hour.ToString("00") + ":" + minute.ToString("00");
    }
    
    
    
}