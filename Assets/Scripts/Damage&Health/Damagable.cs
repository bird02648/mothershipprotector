

namespace Damage_Health
{
   public interface IDamage
   {
      void TakeDamage(int damage);
      
   }
}
