namespace Damage_Health
{
    public interface IHeal 
    {
        void TakeHeal(int heal);
    }
}
