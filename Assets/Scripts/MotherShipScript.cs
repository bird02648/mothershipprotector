using System.Collections;
using System.Collections.Generic;
using Damage_Health;
using UnityEngine;
using UnityEngine.UI;

public class MotherShipScript : MonoBehaviour,IHeal,IDamage
{
    public Text healthText;
    public Image healthBar;
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;
    [SerializeField] GameObject gameOverUI;
    [SerializeField] private AudioClip motherShipExplodedSounds;
    [SerializeField] private float motherShipExplodedSoundsValume = 0.3f;
    [SerializeField] private SpriteRenderer sprite;



    private void Start()
    {
        health = maxHealth;
        Time.timeScale  = 1 ;
    }

    private void Update()
    {
        healthText.text = "Health: " + health + " /"+maxHealth;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        
        HealthBarFiller();
    }

    void HealthBarFiller()
    {
        healthBar.fillAmount = health / maxHealth;
    }
    
    public void TakeHeal(int heal)
    {
        if (health < maxHealth)
        {
            health += heal ;
        }
    }

    public void TakeDamage(int damage)
    {
        StartCoroutine(FlashRed());
        if (health > 0)
        {
            health -= damage;
        }
        if (health <= 0)
        {
            AudioSource.PlayClipAtPoint(motherShipExplodedSounds,Camera.main.transform.position, motherShipExplodedSoundsValume);
            gameOverUI.SetActive(true);
            Time.timeScale = 0 ;
        }
       
    }

    public IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }

    



}
