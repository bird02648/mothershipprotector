using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private SpawnObject spawnObject;

    private void Start()
    {
        spawnObject = GameObject.Find("SpawnItem").GetComponent<SpawnObject>();
        spawnObject.StartSpawning();
    }
}
