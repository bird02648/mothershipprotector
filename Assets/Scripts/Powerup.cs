using Damage_Health;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    [SerializeField]private int powerupID;
    [SerializeField]private float speed = 3.0f;



    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        if( transform.position.y< -8f)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            PlayerController player = other.transform.GetComponent<PlayerController>();
            if (player != null)
            {
                switch (powerupID)
                {
                    case 0:
                        player.TripleShotActive();
                        break;
                    case 1:
                        player.SpeedBoostActive();
                        break;
                    case 2:
                        player.ShieldIsActive();
                        break;
                    case 3:
                        player.IncreaseLives();
                        break;
                    default:
                        Debug.Log("Default");
                        break;
                }
            }

            Destroy(this.gameObject);

        }

        if (other.tag == "MotherShip")
        {
            Destroy(gameObject);
            
        }
    }
}
