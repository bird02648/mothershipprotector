using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundsOnClicked : MonoBehaviour
{
    [SerializeField] private Button Button;
    [SerializeField] private AudioClip clickedSounds;
    [SerializeField] private float clickedSoundsValume = 0.1f;

    private void Awake()
    {
        Button.onClick.AddListener(PlaySounds);
    }



    private void PlaySounds()
    {
        AudioSource.PlayClipAtPoint(clickedSounds,Camera.main.transform.position,clickedSoundsValume);
    }
    
}
