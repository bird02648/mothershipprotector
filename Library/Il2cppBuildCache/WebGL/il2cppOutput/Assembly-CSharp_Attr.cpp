﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_damage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_moveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_bomb(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_bombExplodeSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_bombExplodeSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_explosionGo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_projectileSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_projectile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_maxAmmo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_ammoRelaodTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_TripleShoot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_Bullet_TripleShotPowerDownRoutine_mC16EB53AD5998976B857A5120C49021518FCFE7F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_0_0_0_var), NULL);
	}
}
static void U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15__ctor_mCFF62AE12A6B232635118E1FBCE6D33E5AF80B3E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_IDisposable_Dispose_m72864D9652F08228FCCA96DAC5F2C33D914EC934(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE331FA26D1DD279EAAEC9A300E78A4C357601F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_Reset_m98A5249901CABFCCB50A65EAEE87DEC09BB1BE8F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_get_Current_mE14C543A7E18A777E1ECC3D2AA75D3FFC9554186(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_maxHealth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_gameOverUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_motherShipExplodedSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_motherShipExplodedSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_sprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_MotherShipScript_FlashRed_m487AFF57F5DB9E59C0C22183CDA64616DC81311E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_0_0_0_var), NULL);
	}
}
static void U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13__ctor_m1138E4A69DDA84BB9B56D5A8009FBB9384C60FE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_IDisposable_Dispose_m02209FDA39CFB67F86AFE99446F813473A8539A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDEE3F7725D167ABFE541B80F396AB9D321C2A04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_Collections_IEnumerator_Reset_mA932ED022EFDC288C972EB016C8CBC115994D9DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_Collections_IEnumerator_get_Current_mB8CD7533328B487B4F06DBF959204C71CD4B2209(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MoveProjectile_tE0D0EFD57DA6E28B0E476D198751B32E4717C240_CustomAttributesCacheGenerator_projectile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveProjectile_tE0D0EFD57DA6E28B0E476D198751B32E4717C240_CustomAttributesCacheGenerator_moveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_projectileSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_projectileTripleSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_projectile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_maxAmmo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_ammoReloadTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_TripleShoot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_Shield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_speedMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_speedDevider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_life(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerFireSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerFireSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_powerUpSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_powerUpSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerExplodeSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerExplodeSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadEmptySounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadEmptyValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_SpeedBoostPowerDownRoutine_m3F5FDEFDD74289402AAEC04E7D6D1A8CFFDC4D83(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_0_0_0_var), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_CantMove_m97A46C7AC1FA6F10B95FFB7B27EBB5236015430E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_0_0_0_var), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_SpeedPowerDownRoutine_m337CA06F16E2BB6B5B278ADF3178CC22372BD0C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_0_0_0_var), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_FlashRed_m6DD65E9D812CEDC870DF0789B07921881714E456(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_0_0_0_var), NULL);
	}
}
static void U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46__ctor_m2462DCB510ADE67DB709BD18355F887C804227AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_IDisposable_Dispose_m244E90AB4738D03FAC75B4652581226B6216BD01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BE677BF10E452A2C518EF361094AC844A2344ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_Reset_m0E940574D66056147142B2C7E88FAD7664DCE0F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_get_Current_m9DB9A969F524FF624C91CDE748860451F543337E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49__ctor_mA65038E100EACA333153B17AACED8B11252D6E57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_IDisposable_Dispose_m92B6EC327897DBE6A341924514A26D709A27BCFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC0FC86AAABE04418399F20D7937451008951DC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_Collections_IEnumerator_Reset_m97965F5E7C82AF17D89070C8FD2C84FDA8D31837(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_Collections_IEnumerator_get_Current_m4E0B386F2BFEFBF99F590A32F7D1DEDEB9A9A7BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51__ctor_m9558ADA773E4EA94511CD866271416CB9477AFF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_IDisposable_Dispose_m0C27FCC4843EA416CE242F5A4F00B7F02483B1C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m595340DA92C792F1CCF4AD6D677E712C29A19EB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_Reset_m3CCC5ADA7F91F99E72B61F26CBCF15061A3844AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_get_Current_m1B18042D5CFAE4FC84C22A500AFAA767218FD1B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53__ctor_m8C0AC51E486C66DA196931EA4BF4A49D8D7B75E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_IDisposable_Dispose_m278CD5C5AF4A252758F37CDC58CAE7A7DD64D806(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7C1B67B0CAC2E9503B5CD059B8258E9DB978033(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_Collections_IEnumerator_Reset_m8CAAFE8F3FBA74F3CC7370F970C88D04EF60ABE1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_Collections_IEnumerator_get_Current_m9423697AA5E956DF690246D6177092329CCB3136(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PowerDown_t88D6F35B23B71C005613E549085CBF0411F8F3A1_CustomAttributesCacheGenerator_powerDownID(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PowerDown_t88D6F35B23B71C005613E549085CBF0411F8F3A1_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Powerup_t1D4F23DF6CDB4E3979A5EF7570DF003354E387E5_CustomAttributesCacheGenerator_powerupID(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Powerup_t1D4F23DF6CDB4E3979A5EF7570DF003354E387E5_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundsOnClicked_t033F402A0C235AD4458D4EC4D04DCE632CF99271_CustomAttributesCacheGenerator_Button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundsOnClicked_t033F402A0C235AD4458D4EC4D04DCE632CF99271_CustomAttributesCacheGenerator_clickedSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundsOnClicked_t033F402A0C235AD4458D4EC4D04DCE632CF99271_CustomAttributesCacheGenerator_clickedSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_bomb(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_maxX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_minX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_maxY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_minY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_timeBetweenSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_spawnBombSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_spawnBombSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_powerUps(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_powerDowns(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_SpawnPowerUpRoutine_m884007A08957602BBBB4352CA7A3329F4049BD7E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_0_0_0_var), NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_SpawnPowerDownRoutine_m68AB3868BEA37BF735337B13C2EF323BD8F6430D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_0_0_0_var), NULL);
	}
}
static void U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4__ctor_m32567B76F5B4D2D3AC7CDEEFDB5746599488CD85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_IDisposable_Dispose_m2F4D0625B3B06A44C3406B1985735F6E97E1719C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABAE9A866A430D6E168CEB5FB633ABEC8F774A89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m8BF715C8CFEE96233386EB674F716E84C17F74EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m5C79129174EB642C5143919324CBD8B7AD84BCA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5__ctor_m64BA4F8AE7314E1DEE5873FB7587C659B83F35F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_IDisposable_Dispose_m57295631E0BD2925CBE1FBEE7A1F65426008C9AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m976613FD895AE447CB2E35AAD75A67587B05976F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m59E04556F4B092B3FEFC3859307A35225167EEE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_m5752F0CA31DA9146D265F27A579332D065423BC9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_textTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_time(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_winSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_winsoundValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_winGameUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeEndless_t91E9FA68F16724CC88C6C90792CADF33C301C60F_CustomAttributesCacheGenerator_textTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeEndless_t91E9FA68F16724CC88C6C90792CADF33C301C60F_CustomAttributesCacheGenerator_time(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeEndless_t91E9FA68F16724CC88C6C90792CADF33C301C60F_CustomAttributesCacheGenerator_textTimeShow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ZigZagMovement_tB23D508FFAA4764302279C08CAA1495AF94AAF00_CustomAttributesCacheGenerator_enemy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ZigZagMovement_tB23D508FFAA4764302279C08CAA1495AF94AAF00_CustomAttributesCacheGenerator_totalChange(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ZigZagMovement_tB23D508FFAA4764302279C08CAA1495AF94AAF00_CustomAttributesCacheGenerator_changeInPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_optionPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_mainMenuPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_creditPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_howToPlay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_mode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_clickedSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_clickedSoundsValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tF7E9057AA85F912978AE546B555B4EAAF0FD1A36_CustomAttributesCacheGenerator_pauseUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tF7E9057AA85F912978AE546B555B4EAAF0FD1A36_CustomAttributesCacheGenerator_clickedSounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tF7E9057AA85F912978AE546B555B4EAAF0FD1A36_CustomAttributesCacheGenerator_clickedSoundsValume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectMode_tFB5982AB74EA3D051ABB9C9E08083895DFB51CF8_CustomAttributesCacheGenerator_normalMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectMode_tFB5982AB74EA3D051ABB9C9E08083895DFB51CF8_CustomAttributesCacheGenerator_endlessMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectMode_tFB5982AB74EA3D051ABB9C9E08083895DFB51CF8_CustomAttributesCacheGenerator_backToMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator__LiveImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator__liveSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator_gameOverUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator_UIManager_U3CUpdateLivesU3Eg__GameOverSequenceU7C3_0_mEFFF8E941B8AC5CE5557B02453B376B04422322E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[142] = 
{
	U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator,
	U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator,
	U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator,
	U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator,
	U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator,
	U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator,
	U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator,
	Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_damage,
	Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_moveSpeed,
	Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_bomb,
	Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_bombExplodeSounds,
	Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_bombExplodeSoundsValume,
	Bomb_t2F20FE59E0B0F4EE5ED5E76D2DD8406D9487CCC5_CustomAttributesCacheGenerator_explosionGo,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_projectileSpawn,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_projectile,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_maxAmmo,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_ammoRelaodTime,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_TripleShoot,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_health,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_maxHealth,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_gameOverUI,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_motherShipExplodedSounds,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_motherShipExplodedSoundsValume,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_sprite,
	MoveProjectile_tE0D0EFD57DA6E28B0E476D198751B32E4717C240_CustomAttributesCacheGenerator_projectile,
	MoveProjectile_tE0D0EFD57DA6E28B0E476D198751B32E4717C240_CustomAttributesCacheGenerator_moveSpeed,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerSpeed,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_projectileSpawn,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_projectileTripleSpawn,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_projectile,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_maxAmmo,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_ammoReloadTime,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_TripleShoot,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_Shield,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_speedMultiplier,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_speedDevider,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_life,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerFireSounds,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerFireSoundsValume,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_powerUpSounds,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_powerUpSoundsValume,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerExplodeSounds,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playerExplodeSoundsValume,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadSounds,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadValume,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadEmptySounds,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_reloadEmptyValume,
	PowerDown_t88D6F35B23B71C005613E549085CBF0411F8F3A1_CustomAttributesCacheGenerator_powerDownID,
	PowerDown_t88D6F35B23B71C005613E549085CBF0411F8F3A1_CustomAttributesCacheGenerator_speed,
	Powerup_t1D4F23DF6CDB4E3979A5EF7570DF003354E387E5_CustomAttributesCacheGenerator_powerupID,
	Powerup_t1D4F23DF6CDB4E3979A5EF7570DF003354E387E5_CustomAttributesCacheGenerator_speed,
	SoundsOnClicked_t033F402A0C235AD4458D4EC4D04DCE632CF99271_CustomAttributesCacheGenerator_Button,
	SoundsOnClicked_t033F402A0C235AD4458D4EC4D04DCE632CF99271_CustomAttributesCacheGenerator_clickedSounds,
	SoundsOnClicked_t033F402A0C235AD4458D4EC4D04DCE632CF99271_CustomAttributesCacheGenerator_clickedSoundsValume,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_bomb,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_maxX,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_minX,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_maxY,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_minY,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_timeBetweenSpawn,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_spawnBombSounds,
	SpawnBomb_t3295F4439C0B3026872CF4E353186D764E2E85ED_CustomAttributesCacheGenerator_spawnBombSoundsValume,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_powerUps,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_powerDowns,
	TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_textTimer,
	TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_time,
	TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_winSounds,
	TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_winsoundValume,
	TimeCount_tFCEC320E1D7250A476B8C01079F5DCA6F4780B0B_CustomAttributesCacheGenerator_winGameUI,
	TimeEndless_t91E9FA68F16724CC88C6C90792CADF33C301C60F_CustomAttributesCacheGenerator_textTimer,
	TimeEndless_t91E9FA68F16724CC88C6C90792CADF33C301C60F_CustomAttributesCacheGenerator_time,
	TimeEndless_t91E9FA68F16724CC88C6C90792CADF33C301C60F_CustomAttributesCacheGenerator_textTimeShow,
	ZigZagMovement_tB23D508FFAA4764302279C08CAA1495AF94AAF00_CustomAttributesCacheGenerator_enemy,
	ZigZagMovement_tB23D508FFAA4764302279C08CAA1495AF94AAF00_CustomAttributesCacheGenerator_totalChange,
	ZigZagMovement_tB23D508FFAA4764302279C08CAA1495AF94AAF00_CustomAttributesCacheGenerator_changeInPosition,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_optionPanel,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_mainMenuPanel,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_creditPanel,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_howToPlay,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_mode,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_clickedSounds,
	MainMenu_tDC081FD41A81E74FD3EB756F8006FC7687EA332A_CustomAttributesCacheGenerator_clickedSoundsValue,
	PauseMenu_tF7E9057AA85F912978AE546B555B4EAAF0FD1A36_CustomAttributesCacheGenerator_pauseUI,
	PauseMenu_tF7E9057AA85F912978AE546B555B4EAAF0FD1A36_CustomAttributesCacheGenerator_clickedSounds,
	PauseMenu_tF7E9057AA85F912978AE546B555B4EAAF0FD1A36_CustomAttributesCacheGenerator_clickedSoundsValume,
	SelectMode_tFB5982AB74EA3D051ABB9C9E08083895DFB51CF8_CustomAttributesCacheGenerator_normalMode,
	SelectMode_tFB5982AB74EA3D051ABB9C9E08083895DFB51CF8_CustomAttributesCacheGenerator_endlessMode,
	SelectMode_tFB5982AB74EA3D051ABB9C9E08083895DFB51CF8_CustomAttributesCacheGenerator_backToMenu,
	UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator__LiveImage,
	UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator__liveSprite,
	UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator_gameOverUI,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_Bullet_TripleShotPowerDownRoutine_mC16EB53AD5998976B857A5120C49021518FCFE7F,
	U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15__ctor_mCFF62AE12A6B232635118E1FBCE6D33E5AF80B3E,
	U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_IDisposable_Dispose_m72864D9652F08228FCCA96DAC5F2C33D914EC934,
	U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE331FA26D1DD279EAAEC9A300E78A4C357601F0,
	U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_Reset_m98A5249901CABFCCB50A65EAEE87DEC09BB1BE8F,
	U3CTripleShotPowerDownRoutineU3Ed__15_tFB1C136DA7D3F086FD576A1FCEA560DCB3967B88_CustomAttributesCacheGenerator_U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_get_Current_mE14C543A7E18A777E1ECC3D2AA75D3FFC9554186,
	MotherShipScript_tA0DC57D624B32AA4E5E23EC3E39A34420EE17B69_CustomAttributesCacheGenerator_MotherShipScript_FlashRed_m487AFF57F5DB9E59C0C22183CDA64616DC81311E,
	U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13__ctor_m1138E4A69DDA84BB9B56D5A8009FBB9384C60FE5,
	U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_IDisposable_Dispose_m02209FDA39CFB67F86AFE99446F813473A8539A8,
	U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDEE3F7725D167ABFE541B80F396AB9D321C2A04,
	U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_Collections_IEnumerator_Reset_mA932ED022EFDC288C972EB016C8CBC115994D9DE,
	U3CFlashRedU3Ed__13_tCDA419E6747D3F3B6611B8AAEDFBB172449C6DC0_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__13_System_Collections_IEnumerator_get_Current_mB8CD7533328B487B4F06DBF959204C71CD4B2209,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_SpeedBoostPowerDownRoutine_m3F5FDEFDD74289402AAEC04E7D6D1A8CFFDC4D83,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_CantMove_m97A46C7AC1FA6F10B95FFB7B27EBB5236015430E,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_SpeedPowerDownRoutine_m337CA06F16E2BB6B5B278ADF3178CC22372BD0C9,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_PlayerController_FlashRed_m6DD65E9D812CEDC870DF0789B07921881714E456,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46__ctor_m2462DCB510ADE67DB709BD18355F887C804227AA,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_IDisposable_Dispose_m244E90AB4738D03FAC75B4652581226B6216BD01,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BE677BF10E452A2C518EF361094AC844A2344ED,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_Reset_m0E940574D66056147142B2C7E88FAD7664DCE0F4,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_t880BD47AB1922620DA374269D84CC4D1B6525910_CustomAttributesCacheGenerator_U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_get_Current_m9DB9A969F524FF624C91CDE748860451F543337E,
	U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49__ctor_mA65038E100EACA333153B17AACED8B11252D6E57,
	U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_IDisposable_Dispose_m92B6EC327897DBE6A341924514A26D709A27BCFB,
	U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC0FC86AAABE04418399F20D7937451008951DC0,
	U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_Collections_IEnumerator_Reset_m97965F5E7C82AF17D89070C8FD2C84FDA8D31837,
	U3CCantMoveU3Ed__49_tA2D610E9E3E41CF4A950FC3C0F3DC136F00C15EB_CustomAttributesCacheGenerator_U3CCantMoveU3Ed__49_System_Collections_IEnumerator_get_Current_m4E0B386F2BFEFBF99F590A32F7D1DEDEB9A9A7BA,
	U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51__ctor_m9558ADA773E4EA94511CD866271416CB9477AFF7,
	U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_IDisposable_Dispose_m0C27FCC4843EA416CE242F5A4F00B7F02483B1C2,
	U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m595340DA92C792F1CCF4AD6D677E712C29A19EB2,
	U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_Reset_m3CCC5ADA7F91F99E72B61F26CBCF15061A3844AB,
	U3CSpeedPowerDownRoutineU3Ed__51_t1BBFA6F14742AAA9BD8A2B07C61459FF1C92D3E8_CustomAttributesCacheGenerator_U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_get_Current_m1B18042D5CFAE4FC84C22A500AFAA767218FD1B6,
	U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53__ctor_m8C0AC51E486C66DA196931EA4BF4A49D8D7B75E5,
	U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_IDisposable_Dispose_m278CD5C5AF4A252758F37CDC58CAE7A7DD64D806,
	U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7C1B67B0CAC2E9503B5CD059B8258E9DB978033,
	U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_Collections_IEnumerator_Reset_m8CAAFE8F3FBA74F3CC7370F970C88D04EF60ABE1,
	U3CFlashRedU3Ed__53_tEE94E3594B9E615698916646A40D2211B0D58D73_CustomAttributesCacheGenerator_U3CFlashRedU3Ed__53_System_Collections_IEnumerator_get_Current_m9423697AA5E956DF690246D6177092329CCB3136,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_SpawnPowerUpRoutine_m884007A08957602BBBB4352CA7A3329F4049BD7E,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_SpawnPowerDownRoutine_m68AB3868BEA37BF735337B13C2EF323BD8F6430D,
	U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4__ctor_m32567B76F5B4D2D3AC7CDEEFDB5746599488CD85,
	U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_IDisposable_Dispose_m2F4D0625B3B06A44C3406B1985735F6E97E1719C,
	U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABAE9A866A430D6E168CEB5FB633ABEC8F774A89,
	U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m8BF715C8CFEE96233386EB674F716E84C17F74EA,
	U3CSpawnPowerUpRoutineU3Ed__4_t5154F5345CC45AD917D95EE7BED3E044546513CD_CustomAttributesCacheGenerator_U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m5C79129174EB642C5143919324CBD8B7AD84BCA9,
	U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5__ctor_m64BA4F8AE7314E1DEE5873FB7587C659B83F35F9,
	U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_IDisposable_Dispose_m57295631E0BD2925CBE1FBEE7A1F65426008C9AE,
	U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m976613FD895AE447CB2E35AAD75A67587B05976F,
	U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m59E04556F4B092B3FEFC3859307A35225167EEE3,
	U3CSpawnPowerDownRoutineU3Ed__5_t263C1D49495FAE70BB82640722CF1B83F98893D0_CustomAttributesCacheGenerator_U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_m5752F0CA31DA9146D265F27A579332D065423BC9,
	UIManager_t2831E74CA6D141E06A2AB44D71662ABC3C72DEF1_CustomAttributesCacheGenerator_UIManager_U3CUpdateLivesU3Eg__GameOverSequenceU7C3_0_mEFFF8E941B8AC5CE5557B02453B376B04422322E,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
