﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bomb::Start()
extern void Bomb_Start_m65DC0C3B6815EC2CBEACB4790307FF0A063F3BA3 (void);
// 0x00000002 System.Void Bomb::Update()
extern void Bomb_Update_m966A0500B424067F8B88D58661431AB473FDFA62 (void);
// 0x00000003 System.Void Bomb::Awake()
extern void Bomb_Awake_m3CAE1647F328E5892A740D9B204F45CD6151FE7B (void);
// 0x00000004 System.Void Bomb::PlayExplosion()
extern void Bomb_PlayExplosion_m17C7AD95CD8B1219E34C75B4996641E86ABBCD5D (void);
// 0x00000005 System.Void Bomb::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bomb_OnTriggerEnter2D_mA28DDFBF317D52558D3C862B97FE2CCACD536C4B (void);
// 0x00000006 System.Void Bomb::.ctor()
extern void Bomb__ctor_mB4A4B674437E391EA003374752A044DD5E539C46 (void);
// 0x00000007 System.Void Bullet::Start()
extern void Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4 (void);
// 0x00000008 System.Void Bullet::Update()
extern void Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B (void);
// 0x00000009 System.Void Bullet::Text()
extern void Bullet_Text_m7733CEA820EF2D05DB168B6E40760521B413025A (void);
// 0x0000000A System.Void Bullet::Shoot()
extern void Bullet_Shoot_mC17C669FC279C48BCCED1FA00A95A48200888F09 (void);
// 0x0000000B System.Void Bullet::Reload()
extern void Bullet_Reload_mAD873871073566D5561CD9479A3C3C548A016ABA (void);
// 0x0000000C System.Void Bullet::TripleShotActive()
extern void Bullet_TripleShotActive_m33A4CFC198EB7A5E0C00E2CC9C7B70BF78E3CEF8 (void);
// 0x0000000D System.Collections.IEnumerator Bullet::TripleShotPowerDownRoutine()
extern void Bullet_TripleShotPowerDownRoutine_mC16EB53AD5998976B857A5120C49021518FCFE7F (void);
// 0x0000000E System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x0000000F System.Void Bullet/<TripleShotPowerDownRoutine>d__15::.ctor(System.Int32)
extern void U3CTripleShotPowerDownRoutineU3Ed__15__ctor_mCFF62AE12A6B232635118E1FBCE6D33E5AF80B3E (void);
// 0x00000010 System.Void Bullet/<TripleShotPowerDownRoutine>d__15::System.IDisposable.Dispose()
extern void U3CTripleShotPowerDownRoutineU3Ed__15_System_IDisposable_Dispose_m72864D9652F08228FCCA96DAC5F2C33D914EC934 (void);
// 0x00000011 System.Boolean Bullet/<TripleShotPowerDownRoutine>d__15::MoveNext()
extern void U3CTripleShotPowerDownRoutineU3Ed__15_MoveNext_m39E3AB649849776660B9C12FB61D48AC62DB0FF3 (void);
// 0x00000012 System.Object Bullet/<TripleShotPowerDownRoutine>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE331FA26D1DD279EAAEC9A300E78A4C357601F0 (void);
// 0x00000013 System.Void Bullet/<TripleShotPowerDownRoutine>d__15::System.Collections.IEnumerator.Reset()
extern void U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_Reset_m98A5249901CABFCCB50A65EAEE87DEC09BB1BE8F (void);
// 0x00000014 System.Object Bullet/<TripleShotPowerDownRoutine>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_get_Current_mE14C543A7E18A777E1ECC3D2AA75D3FFC9554186 (void);
// 0x00000015 System.Void Destroyer::DestroyGameObject()
extern void Destroyer_DestroyGameObject_m84048A257C319F6A5E6E2A977FFB15EDD58403E2 (void);
// 0x00000016 System.Void Destroyer::.ctor()
extern void Destroyer__ctor_m6C3428ACE88A6900012F8574B8C554E331B3ED15 (void);
// 0x00000017 System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x00000018 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000019 System.Void MotherShipScript::Start()
extern void MotherShipScript_Start_m13E8651BD0C2B700142241A88CA83B7DAA8B1454 (void);
// 0x0000001A System.Void MotherShipScript::Update()
extern void MotherShipScript_Update_mCA9BE3383F2F505414E4A54D199986AF5A8B1DF1 (void);
// 0x0000001B System.Void MotherShipScript::HealthBarFiller()
extern void MotherShipScript_HealthBarFiller_mAA666C56BBC695B642644F926533382E82F7C925 (void);
// 0x0000001C System.Void MotherShipScript::TakeHeal(System.Int32)
extern void MotherShipScript_TakeHeal_m0CBCDE043ABF19070FE02AEBF805967F1D32F7AB (void);
// 0x0000001D System.Void MotherShipScript::TakeDamage(System.Int32)
extern void MotherShipScript_TakeDamage_mCD24DF0E692FCEB6FBB50EC876B051E1B6F8277D (void);
// 0x0000001E System.Collections.IEnumerator MotherShipScript::FlashRed()
extern void MotherShipScript_FlashRed_m487AFF57F5DB9E59C0C22183CDA64616DC81311E (void);
// 0x0000001F System.Void MotherShipScript::.ctor()
extern void MotherShipScript__ctor_m49F0BF76DA2138873F5D5C4AC2123E30AEA4C866 (void);
// 0x00000020 System.Void MotherShipScript/<FlashRed>d__13::.ctor(System.Int32)
extern void U3CFlashRedU3Ed__13__ctor_m1138E4A69DDA84BB9B56D5A8009FBB9384C60FE5 (void);
// 0x00000021 System.Void MotherShipScript/<FlashRed>d__13::System.IDisposable.Dispose()
extern void U3CFlashRedU3Ed__13_System_IDisposable_Dispose_m02209FDA39CFB67F86AFE99446F813473A8539A8 (void);
// 0x00000022 System.Boolean MotherShipScript/<FlashRed>d__13::MoveNext()
extern void U3CFlashRedU3Ed__13_MoveNext_m752AC448D4B43D345809F9A20FA88802AD6DC875 (void);
// 0x00000023 System.Object MotherShipScript/<FlashRed>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlashRedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDEE3F7725D167ABFE541B80F396AB9D321C2A04 (void);
// 0x00000024 System.Void MotherShipScript/<FlashRed>d__13::System.Collections.IEnumerator.Reset()
extern void U3CFlashRedU3Ed__13_System_Collections_IEnumerator_Reset_mA932ED022EFDC288C972EB016C8CBC115994D9DE (void);
// 0x00000025 System.Object MotherShipScript/<FlashRed>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CFlashRedU3Ed__13_System_Collections_IEnumerator_get_Current_mB8CD7533328B487B4F06DBF959204C71CD4B2209 (void);
// 0x00000026 System.Void MoveProjectile::Start()
extern void MoveProjectile_Start_m36984DD1532E94808C1798DC0DA641CAD76BA6C1 (void);
// 0x00000027 System.Void MoveProjectile::Update()
extern void MoveProjectile_Update_m24E4CA48969C2163E11FDAB5361179E68749B947 (void);
// 0x00000028 System.Void MoveProjectile::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void MoveProjectile_OnTriggerEnter2D_mF743762B0645193CB8630042758CAC2419B0B6E4 (void);
// 0x00000029 System.Void MoveProjectile::.ctor()
extern void MoveProjectile__ctor_m60AF44BF1310461A77848050AA00AE2EFE6C6A51 (void);
// 0x0000002A System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x0000002B System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x0000002C System.Void PlayerController::TripleShootTextCheck()
extern void PlayerController_TripleShootTextCheck_mEA7E47027A7E612139C1A57EF8A5E1E25518A315 (void);
// 0x0000002D System.Void PlayerController::Shoot()
extern void PlayerController_Shoot_m5CB8DCC4363201A5461EA10BD9A248642A2676D8 (void);
// 0x0000002E System.Void PlayerController::TextAmmo()
extern void PlayerController_TextAmmo_m763E5AB113832EA0E01C8CAA625BCCCC4FDC984F (void);
// 0x0000002F System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m54EE3ADAA7597303B1F69849B233D1A68D880B14 (void);
// 0x00000030 System.Void PlayerController::Reload()
extern void PlayerController_Reload_m1B3AA594C0785D790818B1A8875DA078F2717850 (void);
// 0x00000031 System.Void PlayerController::Damage()
extern void PlayerController_Damage_m704707F3EDAD03FFC6575A175ABB6AE3FA774CD3 (void);
// 0x00000032 System.Void PlayerController::TripleShotActive()
extern void PlayerController_TripleShotActive_m93CF64DA76F61147E0F4F17A42C9B27B91DB43DD (void);
// 0x00000033 System.Void PlayerController::TripleShotPowerDownRoutine()
extern void PlayerController_TripleShotPowerDownRoutine_m384EE7F544298590B0F7C47D156A64E5E9A89B75 (void);
// 0x00000034 System.Void PlayerController::SpeedBoostActive()
extern void PlayerController_SpeedBoostActive_m867A04B10D12A82206799CD41AA443E77707329F (void);
// 0x00000035 System.Collections.IEnumerator PlayerController::SpeedBoostPowerDownRoutine()
extern void PlayerController_SpeedBoostPowerDownRoutine_m3F5FDEFDD74289402AAEC04E7D6D1A8CFFDC4D83 (void);
// 0x00000036 System.Void PlayerController::ShieldIsActive()
extern void PlayerController_ShieldIsActive_m7B8760457F4E776241D6295D18ABAAFAC9D142BB (void);
// 0x00000037 System.Void PlayerController::CanMove()
extern void PlayerController_CanMove_m49C1F64D65F4720FE2B8D807F98087DEBF16E5DC (void);
// 0x00000038 System.Collections.IEnumerator PlayerController::CantMove()
extern void PlayerController_CantMove_m97A46C7AC1FA6F10B95FFB7B27EBB5236015430E (void);
// 0x00000039 System.Void PlayerController::SpeedActive()
extern void PlayerController_SpeedActive_m94F0A7E65CE28D8FAA197F1247BFBB05C4A911C1 (void);
// 0x0000003A System.Collections.IEnumerator PlayerController::SpeedPowerDownRoutine()
extern void PlayerController_SpeedPowerDownRoutine_m337CA06F16E2BB6B5B278ADF3178CC22372BD0C9 (void);
// 0x0000003B System.Void PlayerController::IncreaseLives()
extern void PlayerController_IncreaseLives_mC0BFCC92058492683CB5C1A20CB40608E9A15E0F (void);
// 0x0000003C System.Collections.IEnumerator PlayerController::FlashRed()
extern void PlayerController_FlashRed_m6DD65E9D812CEDC870DF0789B07921881714E456 (void);
// 0x0000003D System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x0000003E System.Void PlayerController/<SpeedBoostPowerDownRoutine>d__46::.ctor(System.Int32)
extern void U3CSpeedBoostPowerDownRoutineU3Ed__46__ctor_m2462DCB510ADE67DB709BD18355F887C804227AA (void);
// 0x0000003F System.Void PlayerController/<SpeedBoostPowerDownRoutine>d__46::System.IDisposable.Dispose()
extern void U3CSpeedBoostPowerDownRoutineU3Ed__46_System_IDisposable_Dispose_m244E90AB4738D03FAC75B4652581226B6216BD01 (void);
// 0x00000040 System.Boolean PlayerController/<SpeedBoostPowerDownRoutine>d__46::MoveNext()
extern void U3CSpeedBoostPowerDownRoutineU3Ed__46_MoveNext_m5AE7DB85BC9C8D528916A28175B0E276F23195ED (void);
// 0x00000041 System.Object PlayerController/<SpeedBoostPowerDownRoutine>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BE677BF10E452A2C518EF361094AC844A2344ED (void);
// 0x00000042 System.Void PlayerController/<SpeedBoostPowerDownRoutine>d__46::System.Collections.IEnumerator.Reset()
extern void U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_Reset_m0E940574D66056147142B2C7E88FAD7664DCE0F4 (void);
// 0x00000043 System.Object PlayerController/<SpeedBoostPowerDownRoutine>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_get_Current_m9DB9A969F524FF624C91CDE748860451F543337E (void);
// 0x00000044 System.Void PlayerController/<CantMove>d__49::.ctor(System.Int32)
extern void U3CCantMoveU3Ed__49__ctor_mA65038E100EACA333153B17AACED8B11252D6E57 (void);
// 0x00000045 System.Void PlayerController/<CantMove>d__49::System.IDisposable.Dispose()
extern void U3CCantMoveU3Ed__49_System_IDisposable_Dispose_m92B6EC327897DBE6A341924514A26D709A27BCFB (void);
// 0x00000046 System.Boolean PlayerController/<CantMove>d__49::MoveNext()
extern void U3CCantMoveU3Ed__49_MoveNext_mC0D27A0086BCDEA1CA49CC8682B7D4F0158D3275 (void);
// 0x00000047 System.Object PlayerController/<CantMove>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCantMoveU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC0FC86AAABE04418399F20D7937451008951DC0 (void);
// 0x00000048 System.Void PlayerController/<CantMove>d__49::System.Collections.IEnumerator.Reset()
extern void U3CCantMoveU3Ed__49_System_Collections_IEnumerator_Reset_m97965F5E7C82AF17D89070C8FD2C84FDA8D31837 (void);
// 0x00000049 System.Object PlayerController/<CantMove>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CCantMoveU3Ed__49_System_Collections_IEnumerator_get_Current_m4E0B386F2BFEFBF99F590A32F7D1DEDEB9A9A7BA (void);
// 0x0000004A System.Void PlayerController/<SpeedPowerDownRoutine>d__51::.ctor(System.Int32)
extern void U3CSpeedPowerDownRoutineU3Ed__51__ctor_m9558ADA773E4EA94511CD866271416CB9477AFF7 (void);
// 0x0000004B System.Void PlayerController/<SpeedPowerDownRoutine>d__51::System.IDisposable.Dispose()
extern void U3CSpeedPowerDownRoutineU3Ed__51_System_IDisposable_Dispose_m0C27FCC4843EA416CE242F5A4F00B7F02483B1C2 (void);
// 0x0000004C System.Boolean PlayerController/<SpeedPowerDownRoutine>d__51::MoveNext()
extern void U3CSpeedPowerDownRoutineU3Ed__51_MoveNext_m83538134BCEDBA2EAF314E8AF6FFBC30E247BE42 (void);
// 0x0000004D System.Object PlayerController/<SpeedPowerDownRoutine>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m595340DA92C792F1CCF4AD6D677E712C29A19EB2 (void);
// 0x0000004E System.Void PlayerController/<SpeedPowerDownRoutine>d__51::System.Collections.IEnumerator.Reset()
extern void U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_Reset_m3CCC5ADA7F91F99E72B61F26CBCF15061A3844AB (void);
// 0x0000004F System.Object PlayerController/<SpeedPowerDownRoutine>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_get_Current_m1B18042D5CFAE4FC84C22A500AFAA767218FD1B6 (void);
// 0x00000050 System.Void PlayerController/<FlashRed>d__53::.ctor(System.Int32)
extern void U3CFlashRedU3Ed__53__ctor_m8C0AC51E486C66DA196931EA4BF4A49D8D7B75E5 (void);
// 0x00000051 System.Void PlayerController/<FlashRed>d__53::System.IDisposable.Dispose()
extern void U3CFlashRedU3Ed__53_System_IDisposable_Dispose_m278CD5C5AF4A252758F37CDC58CAE7A7DD64D806 (void);
// 0x00000052 System.Boolean PlayerController/<FlashRed>d__53::MoveNext()
extern void U3CFlashRedU3Ed__53_MoveNext_mB6EDCD7C091D2F6047AD1C00C2F8D7C04B87A31E (void);
// 0x00000053 System.Object PlayerController/<FlashRed>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlashRedU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7C1B67B0CAC2E9503B5CD059B8258E9DB978033 (void);
// 0x00000054 System.Void PlayerController/<FlashRed>d__53::System.Collections.IEnumerator.Reset()
extern void U3CFlashRedU3Ed__53_System_Collections_IEnumerator_Reset_m8CAAFE8F3FBA74F3CC7370F970C88D04EF60ABE1 (void);
// 0x00000055 System.Object PlayerController/<FlashRed>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CFlashRedU3Ed__53_System_Collections_IEnumerator_get_Current_m9423697AA5E956DF690246D6177092329CCB3136 (void);
// 0x00000056 System.Void PowerDown::Update()
extern void PowerDown_Update_m7B0CA441200BF62A9F5E45DDE48E9DBBB675FA96 (void);
// 0x00000057 System.Void PowerDown::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PowerDown_OnTriggerEnter2D_m01653C4D2803504235C0F4B5EB8E3DCBB8704D73 (void);
// 0x00000058 System.Void PowerDown::.ctor()
extern void PowerDown__ctor_m0F9728DDD24EF8227DBED9A799E16E907057FACD (void);
// 0x00000059 System.Void Powerup::Update()
extern void Powerup_Update_mB50D0FFECB14DE8A98B30C9D20515358E9841C01 (void);
// 0x0000005A System.Void Powerup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Powerup_OnTriggerEnter2D_m6C59F2D77A726749A0E682C8D831A276300F21BD (void);
// 0x0000005B System.Void Powerup::.ctor()
extern void Powerup__ctor_m8A07335BF37378BFB09408B62E746F3A9081C6AD (void);
// 0x0000005C System.Void SoundsOnClicked::Awake()
extern void SoundsOnClicked_Awake_mFC3471D0BC3E9163E73DDB4BA50C5F4F736841EF (void);
// 0x0000005D System.Void SoundsOnClicked::PlaySounds()
extern void SoundsOnClicked_PlaySounds_m2941733D05F724D982F7A0BD7072B70138F95FB9 (void);
// 0x0000005E System.Void SoundsOnClicked::.ctor()
extern void SoundsOnClicked__ctor_m985043D2D5206948A0F1BBDFE0E7CCD5F61C6031 (void);
// 0x0000005F System.Void SpawnBomb::Update()
extern void SpawnBomb_Update_m122D910A431AEB9E635437115E19025D4833FF3E (void);
// 0x00000060 System.Void SpawnBomb::Spawn()
extern void SpawnBomb_Spawn_m87E4DEE55DD5C12CBD1CB1F2D759AA98B4739DD2 (void);
// 0x00000061 System.Void SpawnBomb::.ctor()
extern void SpawnBomb__ctor_m5BF0AB536751FF6FE2FAB818E9520480D02B433F (void);
// 0x00000062 System.Void SpawnObject::StartSpawning()
extern void SpawnObject_StartSpawning_m6AD7C7D93D7A7AB81AA31DA12FA9160CFB2CC02A (void);
// 0x00000063 System.Collections.IEnumerator SpawnObject::SpawnPowerUpRoutine()
extern void SpawnObject_SpawnPowerUpRoutine_m884007A08957602BBBB4352CA7A3329F4049BD7E (void);
// 0x00000064 System.Collections.IEnumerator SpawnObject::SpawnPowerDownRoutine()
extern void SpawnObject_SpawnPowerDownRoutine_m68AB3868BEA37BF735337B13C2EF323BD8F6430D (void);
// 0x00000065 System.Void SpawnObject::OnPlayerDeath()
extern void SpawnObject_OnPlayerDeath_mE1D4D7046F42D49641312ABAD02E2835DDCCA531 (void);
// 0x00000066 System.Void SpawnObject::.ctor()
extern void SpawnObject__ctor_m5CDF660BE739E940121CC6E870A389E7F74045EE (void);
// 0x00000067 System.Void SpawnObject/<SpawnPowerUpRoutine>d__4::.ctor(System.Int32)
extern void U3CSpawnPowerUpRoutineU3Ed__4__ctor_m32567B76F5B4D2D3AC7CDEEFDB5746599488CD85 (void);
// 0x00000068 System.Void SpawnObject/<SpawnPowerUpRoutine>d__4::System.IDisposable.Dispose()
extern void U3CSpawnPowerUpRoutineU3Ed__4_System_IDisposable_Dispose_m2F4D0625B3B06A44C3406B1985735F6E97E1719C (void);
// 0x00000069 System.Boolean SpawnObject/<SpawnPowerUpRoutine>d__4::MoveNext()
extern void U3CSpawnPowerUpRoutineU3Ed__4_MoveNext_m6074324E16CBC7C2A2290A397AE22BC87CC8441F (void);
// 0x0000006A System.Object SpawnObject/<SpawnPowerUpRoutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABAE9A866A430D6E168CEB5FB633ABEC8F774A89 (void);
// 0x0000006B System.Void SpawnObject/<SpawnPowerUpRoutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m8BF715C8CFEE96233386EB674F716E84C17F74EA (void);
// 0x0000006C System.Object SpawnObject/<SpawnPowerUpRoutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m5C79129174EB642C5143919324CBD8B7AD84BCA9 (void);
// 0x0000006D System.Void SpawnObject/<SpawnPowerDownRoutine>d__5::.ctor(System.Int32)
extern void U3CSpawnPowerDownRoutineU3Ed__5__ctor_m64BA4F8AE7314E1DEE5873FB7587C659B83F35F9 (void);
// 0x0000006E System.Void SpawnObject/<SpawnPowerDownRoutine>d__5::System.IDisposable.Dispose()
extern void U3CSpawnPowerDownRoutineU3Ed__5_System_IDisposable_Dispose_m57295631E0BD2925CBE1FBEE7A1F65426008C9AE (void);
// 0x0000006F System.Boolean SpawnObject/<SpawnPowerDownRoutine>d__5::MoveNext()
extern void U3CSpawnPowerDownRoutineU3Ed__5_MoveNext_mB4691A8BFDD22A931230250CFFBCA81D6976C14D (void);
// 0x00000070 System.Object SpawnObject/<SpawnPowerDownRoutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m976613FD895AE447CB2E35AAD75A67587B05976F (void);
// 0x00000071 System.Void SpawnObject/<SpawnPowerDownRoutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m59E04556F4B092B3FEFC3859307A35225167EEE3 (void);
// 0x00000072 System.Object SpawnObject/<SpawnPowerDownRoutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_m5752F0CA31DA9146D265F27A579332D065423BC9 (void);
// 0x00000073 System.Void TimeCount::Update()
extern void TimeCount_Update_m597F9BCB4702633579F7FCFFC5F8EF15CFDD9878 (void);
// 0x00000074 System.Void TimeCount::SetText()
extern void TimeCount_SetText_m8D97E8E1D326030BBF164BB9C3B6EF8B0BC718B8 (void);
// 0x00000075 System.Void TimeCount::.ctor()
extern void TimeCount__ctor_m7DCAB948C70DDD6607CABF92EB9422C8D1C2A5CF (void);
// 0x00000076 System.Void TimeEndless::Update()
extern void TimeEndless_Update_m01FF75DE775FA1A94D5D576182B5A948857976E6 (void);
// 0x00000077 System.Void TimeEndless::SetText()
extern void TimeEndless_SetText_mAF369901B52DA58EF2B6AF276EF5626FFCA7E7D0 (void);
// 0x00000078 System.Void TimeEndless::.ctor()
extern void TimeEndless__ctor_m4E6BEEF05DE58024855DC6CFB7E2B37921D73909 (void);
// 0x00000079 System.Void ZigZagMovement::Start()
extern void ZigZagMovement_Start_m2D223B978D8345B91833A0B81766A9D5CA4A5B33 (void);
// 0x0000007A System.Void ZigZagMovement::Update()
extern void ZigZagMovement_Update_m4AB45BF8EC683252E725D91894EFB72A7283E601 (void);
// 0x0000007B System.Void ZigZagMovement::.ctor()
extern void ZigZagMovement__ctor_mA5EEF93BE8C76E377827EB8E35CE79EA1CEBC06C (void);
// 0x0000007C System.Void UI.MainMenu::StartGame()
extern void MainMenu_StartGame_m290A79AD02FA0698EE891B279C36655060E7C9ED (void);
// 0x0000007D System.Void UI.MainMenu::ShowOptions()
extern void MainMenu_ShowOptions_m7B85E7269034B89BDE4E480F0DC1951E39FE5127 (void);
// 0x0000007E System.Void UI.MainMenu::ShowMainMenu()
extern void MainMenu_ShowMainMenu_m51BF2A2ABCE589EA32FC7CC08C05F8021002DEA9 (void);
// 0x0000007F System.Void UI.MainMenu::ShowCredit()
extern void MainMenu_ShowCredit_mAB9659AFC3E0131F07EA4764746382157C73284B (void);
// 0x00000080 System.Void UI.MainMenu::ShowHowToPlay()
extern void MainMenu_ShowHowToPlay_mAA7EE5D72F1894CE9033F26219AB05953134BB61 (void);
// 0x00000081 System.Void UI.MainMenu::Exit()
extern void MainMenu_Exit_m9C9800F94420584629ED492DEC0CF812E90E3ED7 (void);
// 0x00000082 System.Void UI.MainMenu::.ctor()
extern void MainMenu__ctor_m5A991FFC5308A3F01C886DA594DD0B3347E8632D (void);
// 0x00000083 System.Void UI.PauseMenu::Update()
extern void PauseMenu_Update_mEA5B02D5F8CA85583DC68933D69DE4846D7C2F79 (void);
// 0x00000084 System.Void UI.PauseMenu::SwitchPause()
extern void PauseMenu_SwitchPause_m3D94A67FB2F8E8F06B0DBA5E7F31BFACBCC245CF (void);
// 0x00000085 System.Void UI.PauseMenu::Menu()
extern void PauseMenu_Menu_m53B4B00E8AB254405FEEF0ECD8ED518C066AE026 (void);
// 0x00000086 System.Void UI.PauseMenu::RestartScene1()
extern void PauseMenu_RestartScene1_mC796C075A5B272A7345D00BA195BEFFBFCBD588F (void);
// 0x00000087 System.Void UI.PauseMenu::RestartEndlessScene()
extern void PauseMenu_RestartEndlessScene_m3DA4C50F4B3AE04960522644AF59793E87CE288C (void);
// 0x00000088 System.Void UI.PauseMenu::GoScene2()
extern void PauseMenu_GoScene2_mF55D93EB276F01C9234D4B9E2781E626C9DB48BA (void);
// 0x00000089 System.Void UI.PauseMenu::GoScene3()
extern void PauseMenu_GoScene3_m4ABCA84AE6A0CCF15DEFA173617EE7189ABB7E46 (void);
// 0x0000008A System.Void UI.PauseMenu::.ctor()
extern void PauseMenu__ctor_m4158DE45E56ACD39E1F21214806F75FD4BD357F5 (void);
// 0x0000008B System.Void UI.SelectMode::NormalMode()
extern void SelectMode_NormalMode_mD9E10CBEF49B40AD2BF9BA58C01AECD5AD42E200 (void);
// 0x0000008C System.Void UI.SelectMode::EndlessMode()
extern void SelectMode_EndlessMode_m8AC669CB90336E7072F1529028DD64BBE9E9CBF3 (void);
// 0x0000008D System.Void UI.SelectMode::BackToMainMenu()
extern void SelectMode_BackToMainMenu_mE5A24E54A37D29ABCA2291A5D6D14D337623F175 (void);
// 0x0000008E System.Void UI.SelectMode::.ctor()
extern void SelectMode__ctor_mA294E780B4A29D8BB283198C42F5B855A78951A1 (void);
// 0x0000008F System.Void UI.UIManager::UpdateLives(System.Int32)
extern void UIManager_UpdateLives_m747CC9A685726D0B786D6C0AD96FC7CB31D2FDC3 (void);
// 0x00000090 System.Void UI.UIManager::.ctor()
extern void UIManager__ctor_mC3BB9A3229695EF2BF6DBA5BB369C6FB5B60BCC8 (void);
// 0x00000091 System.Void UI.UIManager::<UpdateLives>g__GameOverSequence|3_0()
extern void UIManager_U3CUpdateLivesU3Eg__GameOverSequenceU7C3_0_mEFFF8E941B8AC5CE5557B02453B376B04422322E (void);
// 0x00000092 System.Void Damage_Health.IDamage::TakeDamage(System.Int32)
// 0x00000093 System.Void Damage_Health.IHeal::TakeHeal(System.Int32)
static Il2CppMethodPointer s_methodPointers[147] = 
{
	Bomb_Start_m65DC0C3B6815EC2CBEACB4790307FF0A063F3BA3,
	Bomb_Update_m966A0500B424067F8B88D58661431AB473FDFA62,
	Bomb_Awake_m3CAE1647F328E5892A740D9B204F45CD6151FE7B,
	Bomb_PlayExplosion_m17C7AD95CD8B1219E34C75B4996641E86ABBCD5D,
	Bomb_OnTriggerEnter2D_mA28DDFBF317D52558D3C862B97FE2CCACD536C4B,
	Bomb__ctor_mB4A4B674437E391EA003374752A044DD5E539C46,
	Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4,
	Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B,
	Bullet_Text_m7733CEA820EF2D05DB168B6E40760521B413025A,
	Bullet_Shoot_mC17C669FC279C48BCCED1FA00A95A48200888F09,
	Bullet_Reload_mAD873871073566D5561CD9479A3C3C548A016ABA,
	Bullet_TripleShotActive_m33A4CFC198EB7A5E0C00E2CC9C7B70BF78E3CEF8,
	Bullet_TripleShotPowerDownRoutine_mC16EB53AD5998976B857A5120C49021518FCFE7F,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	U3CTripleShotPowerDownRoutineU3Ed__15__ctor_mCFF62AE12A6B232635118E1FBCE6D33E5AF80B3E,
	U3CTripleShotPowerDownRoutineU3Ed__15_System_IDisposable_Dispose_m72864D9652F08228FCCA96DAC5F2C33D914EC934,
	U3CTripleShotPowerDownRoutineU3Ed__15_MoveNext_m39E3AB649849776660B9C12FB61D48AC62DB0FF3,
	U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE331FA26D1DD279EAAEC9A300E78A4C357601F0,
	U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_Reset_m98A5249901CABFCCB50A65EAEE87DEC09BB1BE8F,
	U3CTripleShotPowerDownRoutineU3Ed__15_System_Collections_IEnumerator_get_Current_mE14C543A7E18A777E1ECC3D2AA75D3FFC9554186,
	Destroyer_DestroyGameObject_m84048A257C319F6A5E6E2A977FFB15EDD58403E2,
	Destroyer__ctor_m6C3428ACE88A6900012F8574B8C554E331B3ED15,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	MotherShipScript_Start_m13E8651BD0C2B700142241A88CA83B7DAA8B1454,
	MotherShipScript_Update_mCA9BE3383F2F505414E4A54D199986AF5A8B1DF1,
	MotherShipScript_HealthBarFiller_mAA666C56BBC695B642644F926533382E82F7C925,
	MotherShipScript_TakeHeal_m0CBCDE043ABF19070FE02AEBF805967F1D32F7AB,
	MotherShipScript_TakeDamage_mCD24DF0E692FCEB6FBB50EC876B051E1B6F8277D,
	MotherShipScript_FlashRed_m487AFF57F5DB9E59C0C22183CDA64616DC81311E,
	MotherShipScript__ctor_m49F0BF76DA2138873F5D5C4AC2123E30AEA4C866,
	U3CFlashRedU3Ed__13__ctor_m1138E4A69DDA84BB9B56D5A8009FBB9384C60FE5,
	U3CFlashRedU3Ed__13_System_IDisposable_Dispose_m02209FDA39CFB67F86AFE99446F813473A8539A8,
	U3CFlashRedU3Ed__13_MoveNext_m752AC448D4B43D345809F9A20FA88802AD6DC875,
	U3CFlashRedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDEE3F7725D167ABFE541B80F396AB9D321C2A04,
	U3CFlashRedU3Ed__13_System_Collections_IEnumerator_Reset_mA932ED022EFDC288C972EB016C8CBC115994D9DE,
	U3CFlashRedU3Ed__13_System_Collections_IEnumerator_get_Current_mB8CD7533328B487B4F06DBF959204C71CD4B2209,
	MoveProjectile_Start_m36984DD1532E94808C1798DC0DA641CAD76BA6C1,
	MoveProjectile_Update_m24E4CA48969C2163E11FDAB5361179E68749B947,
	MoveProjectile_OnTriggerEnter2D_mF743762B0645193CB8630042758CAC2419B0B6E4,
	MoveProjectile__ctor_m60AF44BF1310461A77848050AA00AE2EFE6C6A51,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_TripleShootTextCheck_mEA7E47027A7E612139C1A57EF8A5E1E25518A315,
	PlayerController_Shoot_m5CB8DCC4363201A5461EA10BD9A248642A2676D8,
	PlayerController_TextAmmo_m763E5AB113832EA0E01C8CAA625BCCCC4FDC984F,
	PlayerController_FixedUpdate_m54EE3ADAA7597303B1F69849B233D1A68D880B14,
	PlayerController_Reload_m1B3AA594C0785D790818B1A8875DA078F2717850,
	PlayerController_Damage_m704707F3EDAD03FFC6575A175ABB6AE3FA774CD3,
	PlayerController_TripleShotActive_m93CF64DA76F61147E0F4F17A42C9B27B91DB43DD,
	PlayerController_TripleShotPowerDownRoutine_m384EE7F544298590B0F7C47D156A64E5E9A89B75,
	PlayerController_SpeedBoostActive_m867A04B10D12A82206799CD41AA443E77707329F,
	PlayerController_SpeedBoostPowerDownRoutine_m3F5FDEFDD74289402AAEC04E7D6D1A8CFFDC4D83,
	PlayerController_ShieldIsActive_m7B8760457F4E776241D6295D18ABAAFAC9D142BB,
	PlayerController_CanMove_m49C1F64D65F4720FE2B8D807F98087DEBF16E5DC,
	PlayerController_CantMove_m97A46C7AC1FA6F10B95FFB7B27EBB5236015430E,
	PlayerController_SpeedActive_m94F0A7E65CE28D8FAA197F1247BFBB05C4A911C1,
	PlayerController_SpeedPowerDownRoutine_m337CA06F16E2BB6B5B278ADF3178CC22372BD0C9,
	PlayerController_IncreaseLives_mC0BFCC92058492683CB5C1A20CB40608E9A15E0F,
	PlayerController_FlashRed_m6DD65E9D812CEDC870DF0789B07921881714E456,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	U3CSpeedBoostPowerDownRoutineU3Ed__46__ctor_m2462DCB510ADE67DB709BD18355F887C804227AA,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_System_IDisposable_Dispose_m244E90AB4738D03FAC75B4652581226B6216BD01,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_MoveNext_m5AE7DB85BC9C8D528916A28175B0E276F23195ED,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BE677BF10E452A2C518EF361094AC844A2344ED,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_Reset_m0E940574D66056147142B2C7E88FAD7664DCE0F4,
	U3CSpeedBoostPowerDownRoutineU3Ed__46_System_Collections_IEnumerator_get_Current_m9DB9A969F524FF624C91CDE748860451F543337E,
	U3CCantMoveU3Ed__49__ctor_mA65038E100EACA333153B17AACED8B11252D6E57,
	U3CCantMoveU3Ed__49_System_IDisposable_Dispose_m92B6EC327897DBE6A341924514A26D709A27BCFB,
	U3CCantMoveU3Ed__49_MoveNext_mC0D27A0086BCDEA1CA49CC8682B7D4F0158D3275,
	U3CCantMoveU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC0FC86AAABE04418399F20D7937451008951DC0,
	U3CCantMoveU3Ed__49_System_Collections_IEnumerator_Reset_m97965F5E7C82AF17D89070C8FD2C84FDA8D31837,
	U3CCantMoveU3Ed__49_System_Collections_IEnumerator_get_Current_m4E0B386F2BFEFBF99F590A32F7D1DEDEB9A9A7BA,
	U3CSpeedPowerDownRoutineU3Ed__51__ctor_m9558ADA773E4EA94511CD866271416CB9477AFF7,
	U3CSpeedPowerDownRoutineU3Ed__51_System_IDisposable_Dispose_m0C27FCC4843EA416CE242F5A4F00B7F02483B1C2,
	U3CSpeedPowerDownRoutineU3Ed__51_MoveNext_m83538134BCEDBA2EAF314E8AF6FFBC30E247BE42,
	U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m595340DA92C792F1CCF4AD6D677E712C29A19EB2,
	U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_Reset_m3CCC5ADA7F91F99E72B61F26CBCF15061A3844AB,
	U3CSpeedPowerDownRoutineU3Ed__51_System_Collections_IEnumerator_get_Current_m1B18042D5CFAE4FC84C22A500AFAA767218FD1B6,
	U3CFlashRedU3Ed__53__ctor_m8C0AC51E486C66DA196931EA4BF4A49D8D7B75E5,
	U3CFlashRedU3Ed__53_System_IDisposable_Dispose_m278CD5C5AF4A252758F37CDC58CAE7A7DD64D806,
	U3CFlashRedU3Ed__53_MoveNext_mB6EDCD7C091D2F6047AD1C00C2F8D7C04B87A31E,
	U3CFlashRedU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7C1B67B0CAC2E9503B5CD059B8258E9DB978033,
	U3CFlashRedU3Ed__53_System_Collections_IEnumerator_Reset_m8CAAFE8F3FBA74F3CC7370F970C88D04EF60ABE1,
	U3CFlashRedU3Ed__53_System_Collections_IEnumerator_get_Current_m9423697AA5E956DF690246D6177092329CCB3136,
	PowerDown_Update_m7B0CA441200BF62A9F5E45DDE48E9DBBB675FA96,
	PowerDown_OnTriggerEnter2D_m01653C4D2803504235C0F4B5EB8E3DCBB8704D73,
	PowerDown__ctor_m0F9728DDD24EF8227DBED9A799E16E907057FACD,
	Powerup_Update_mB50D0FFECB14DE8A98B30C9D20515358E9841C01,
	Powerup_OnTriggerEnter2D_m6C59F2D77A726749A0E682C8D831A276300F21BD,
	Powerup__ctor_m8A07335BF37378BFB09408B62E746F3A9081C6AD,
	SoundsOnClicked_Awake_mFC3471D0BC3E9163E73DDB4BA50C5F4F736841EF,
	SoundsOnClicked_PlaySounds_m2941733D05F724D982F7A0BD7072B70138F95FB9,
	SoundsOnClicked__ctor_m985043D2D5206948A0F1BBDFE0E7CCD5F61C6031,
	SpawnBomb_Update_m122D910A431AEB9E635437115E19025D4833FF3E,
	SpawnBomb_Spawn_m87E4DEE55DD5C12CBD1CB1F2D759AA98B4739DD2,
	SpawnBomb__ctor_m5BF0AB536751FF6FE2FAB818E9520480D02B433F,
	SpawnObject_StartSpawning_m6AD7C7D93D7A7AB81AA31DA12FA9160CFB2CC02A,
	SpawnObject_SpawnPowerUpRoutine_m884007A08957602BBBB4352CA7A3329F4049BD7E,
	SpawnObject_SpawnPowerDownRoutine_m68AB3868BEA37BF735337B13C2EF323BD8F6430D,
	SpawnObject_OnPlayerDeath_mE1D4D7046F42D49641312ABAD02E2835DDCCA531,
	SpawnObject__ctor_m5CDF660BE739E940121CC6E870A389E7F74045EE,
	U3CSpawnPowerUpRoutineU3Ed__4__ctor_m32567B76F5B4D2D3AC7CDEEFDB5746599488CD85,
	U3CSpawnPowerUpRoutineU3Ed__4_System_IDisposable_Dispose_m2F4D0625B3B06A44C3406B1985735F6E97E1719C,
	U3CSpawnPowerUpRoutineU3Ed__4_MoveNext_m6074324E16CBC7C2A2290A397AE22BC87CC8441F,
	U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABAE9A866A430D6E168CEB5FB633ABEC8F774A89,
	U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m8BF715C8CFEE96233386EB674F716E84C17F74EA,
	U3CSpawnPowerUpRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m5C79129174EB642C5143919324CBD8B7AD84BCA9,
	U3CSpawnPowerDownRoutineU3Ed__5__ctor_m64BA4F8AE7314E1DEE5873FB7587C659B83F35F9,
	U3CSpawnPowerDownRoutineU3Ed__5_System_IDisposable_Dispose_m57295631E0BD2925CBE1FBEE7A1F65426008C9AE,
	U3CSpawnPowerDownRoutineU3Ed__5_MoveNext_mB4691A8BFDD22A931230250CFFBCA81D6976C14D,
	U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m976613FD895AE447CB2E35AAD75A67587B05976F,
	U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m59E04556F4B092B3FEFC3859307A35225167EEE3,
	U3CSpawnPowerDownRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_m5752F0CA31DA9146D265F27A579332D065423BC9,
	TimeCount_Update_m597F9BCB4702633579F7FCFFC5F8EF15CFDD9878,
	TimeCount_SetText_m8D97E8E1D326030BBF164BB9C3B6EF8B0BC718B8,
	TimeCount__ctor_m7DCAB948C70DDD6607CABF92EB9422C8D1C2A5CF,
	TimeEndless_Update_m01FF75DE775FA1A94D5D576182B5A948857976E6,
	TimeEndless_SetText_mAF369901B52DA58EF2B6AF276EF5626FFCA7E7D0,
	TimeEndless__ctor_m4E6BEEF05DE58024855DC6CFB7E2B37921D73909,
	ZigZagMovement_Start_m2D223B978D8345B91833A0B81766A9D5CA4A5B33,
	ZigZagMovement_Update_m4AB45BF8EC683252E725D91894EFB72A7283E601,
	ZigZagMovement__ctor_mA5EEF93BE8C76E377827EB8E35CE79EA1CEBC06C,
	MainMenu_StartGame_m290A79AD02FA0698EE891B279C36655060E7C9ED,
	MainMenu_ShowOptions_m7B85E7269034B89BDE4E480F0DC1951E39FE5127,
	MainMenu_ShowMainMenu_m51BF2A2ABCE589EA32FC7CC08C05F8021002DEA9,
	MainMenu_ShowCredit_mAB9659AFC3E0131F07EA4764746382157C73284B,
	MainMenu_ShowHowToPlay_mAA7EE5D72F1894CE9033F26219AB05953134BB61,
	MainMenu_Exit_m9C9800F94420584629ED492DEC0CF812E90E3ED7,
	MainMenu__ctor_m5A991FFC5308A3F01C886DA594DD0B3347E8632D,
	PauseMenu_Update_mEA5B02D5F8CA85583DC68933D69DE4846D7C2F79,
	PauseMenu_SwitchPause_m3D94A67FB2F8E8F06B0DBA5E7F31BFACBCC245CF,
	PauseMenu_Menu_m53B4B00E8AB254405FEEF0ECD8ED518C066AE026,
	PauseMenu_RestartScene1_mC796C075A5B272A7345D00BA195BEFFBFCBD588F,
	PauseMenu_RestartEndlessScene_m3DA4C50F4B3AE04960522644AF59793E87CE288C,
	PauseMenu_GoScene2_mF55D93EB276F01C9234D4B9E2781E626C9DB48BA,
	PauseMenu_GoScene3_m4ABCA84AE6A0CCF15DEFA173617EE7189ABB7E46,
	PauseMenu__ctor_m4158DE45E56ACD39E1F21214806F75FD4BD357F5,
	SelectMode_NormalMode_mD9E10CBEF49B40AD2BF9BA58C01AECD5AD42E200,
	SelectMode_EndlessMode_m8AC669CB90336E7072F1529028DD64BBE9E9CBF3,
	SelectMode_BackToMainMenu_mE5A24E54A37D29ABCA2291A5D6D14D337623F175,
	SelectMode__ctor_mA294E780B4A29D8BB283198C42F5B855A78951A1,
	UIManager_UpdateLives_m747CC9A685726D0B786D6C0AD96FC7CB31D2FDC3,
	UIManager__ctor_mC3BB9A3229695EF2BF6DBA5BB369C6FB5B60BCC8,
	UIManager_U3CUpdateLivesU3Eg__GameOverSequenceU7C3_0_mEFFF8E941B8AC5CE5557B02453B376B04422322E,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[147] = 
{
	1510,
	1510,
	1510,
	1510,
	1264,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1468,
	1510,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1254,
	1254,
	1468,
	1510,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1510,
	1510,
	1264,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1468,
	1510,
	1510,
	1468,
	1510,
	1468,
	1510,
	1468,
	1510,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1510,
	1264,
	1510,
	1510,
	1264,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1468,
	1468,
	1510,
	1510,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1254,
	1510,
	1488,
	1468,
	1510,
	1468,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1510,
	1254,
	1510,
	1510,
	1254,
	1254,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	147,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
